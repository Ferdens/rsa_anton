//
//  RSA.swift
//  EncryptinAnton
//
//  Created by Anton on 24.11.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Foundation

struct  ExtendedEuclid {
    var u1  : Int = 0
    var u2  : Int = 0
    var gcd : Int = 0
    init(u1: Int, u2: Int, gcd: Int) {
    self.u1 = u1
    self.u2 = u2
    self.gcd = gcd
    }
}

class RSAAnton {
    
    private var p   : Int = 0
    private var q   : Int = 0
    private var phi : Int = 0
    var e   : Int = 0
    var d   : Int = 0
    var N   : Int = 0
    
    func keyGeneration() -> (Int,Int,Int){
        var simple = RSAAnton.getNotDivisible()
        self.p = simple[Int(arc4random_uniform(UInt32(simple.count)))]
        self.q = simple[Int(arc4random_uniform(UInt32(simple.count)))]
        self.N = self.p * self.q
        self.phi = (p - 1) * (q - 1)
        var possibleE = RSAAnton.getAllPossibleE(phi: self.phi)
        repeat {
            self.e = possibleE[Int(arc4random_uniform(UInt32(possibleE.count)))]
            let eeResult = RSAAnton.ExtendedEuclidResult(a: self.e % self.phi, b: self.phi)
            self.d = eeResult.u1;
        } while self.d < 0
        return (self.e,self.d,self.N)
    }
    // MARk: Encrypt
    func encrypt(text: String) -> String {
        var outStr = ""
        var byteArr  = [Int]()
        for i in text.utf8{
            byteArr.append(Int(i))
        }
        for value in byteArr {
            let encryptedValue = RSAAnton.moduloPow(value: value, pow: self.e, modulo: self.N);
            outStr += String(encryptedValue) + "|";
        }
        return outStr;
    }
    // MARK: Decode
    func  decode(text: String) -> String {
        let arr = RSAAnton.getDecArrayFromText(text: text)
        var byteArr = [UInt8]()
        for i in arr{
            let decryptedValue = RSAAnton.moduloPow(value: i, pow: self.d, modulo: self.N)
            byteArr.append(UInt8(decryptedValue))
        }
        return String(data: Data.init(bytes: byteArr), encoding: String.Encoding.utf8)!
    }
}

extension RSAAnton {
    // MARK: Euclid func
    class func ExtendedEuclidResult (a: Int, b: Int) -> ExtendedEuclid {
        var u1 = 1
        var u3 = a
        var v1 = 0
        var v3 = b
        while v3 > 0 {
            let q0 = u3 / v3
            let q1 = u3 % v3
            let tmp = v1 * q0
            let tn = u1 - tmp
            u1 = v1
            v1 = tn
            u3 = v3
            v3 = q1
        }
        var tmp2 = u1 * a
        tmp2 = u3 - (tmp2)
        let res = tmp2 / (b)
        let result : ExtendedEuclid  = ExtendedEuclid.init(u1: u1, u2: res, gcd: u3)
        return result
    }
    // MARK: Simple or not func
    class func isSimple (value: Int) -> Bool {
        var isSimple = true
        for i in 2..<value {
            if value % i == 0 {
                isSimple = false
                break
            }
        }
        if isSimple{
            return true
        } else {
            return false }
    }
    
    // MARK: Not Divisivble func
    class func getNotDivisible() -> [Int] {
        var arr = [Int]()
        for x in  2..<256 {
            if isSimple(value: x) {
                arr.append(x)
            }
        }
        return arr
    }
    // MARK: All Posiible "e" keys
    class func getAllPossibleE (phi: Int) -> [Int]{
        var arr = [Int]()
        
        for i in 2..<phi{
            if self.ExtendedEuclidResult(a: i, b: phi).gcd == 1{
                arr.append(i)
            }
        }
        return arr
    }
    // MARK: Key Generation
    class func moduloPow(value: Int ,pow: Int ,modulo: Int) ->Int {
        var result = value
        for _ in 0..<(pow - 1)
        {
            result = (result * value) % modulo
        }
        return result
    }
    // MARK: Get Dec Values From String
    class func getDecArrayFromText(text : String) -> [Int] {
        var i = 0
        var arr = [Int]()
        var tmp = ""
        for c in text.characters
        {
            if c != "|"
            {
                tmp += String(c)
            } else {
                arr.append(Int(tmp)!)
                i += 1
                tmp = ""
            }
        }
        return arr;
    }
}
