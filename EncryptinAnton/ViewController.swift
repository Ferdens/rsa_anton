//
//  ViewController.swift
//  EncryptinAnton
//`zc`zc`xzc`c`c~SC
//  Created by Anton on 24.11.16.
//  Copyright © 2016 Macbook. All rights reserved.
//
// ky ky
import Cocoa

class ViewController: NSViewController {
    
    var myRSA = RSAAnton()
    
    @IBOutlet weak var textOUT        : NSTextField!
    @IBOutlet weak var textIN         : NSTextField!
    @IBOutlet weak var keyE           : NSTextField!
    @IBOutlet weak var keyN           : NSTextField!
    @IBOutlet weak var keyD           : NSTextField!
    @IBOutlet weak var keyNTwo        : NSTextField!
    @IBOutlet weak var encryptDecrypt : NSSegmentedControl!

    // MARK: Clean text fields
    @IBAction func cleanAll(_ sender: NSButton) {
        self.textOUT.stringValue = ""
        self.textIN.stringValue = ""
        self.keyE.stringValue = ""
        self.keyD.stringValue = ""
        self.keyN.stringValue = ""
        self.keyNTwo.stringValue = ""
    }
    
    @IBAction func transferButton(_ sender: NSButton) {
        self.textIN.stringValue = self.textOUT.stringValue
        self.textOUT.stringValue = ""
        if self.encryptDecrypt.isSelected(forSegment: 0){
            self.encryptDecrypt.selectSegment(withTag: 1)
        } else {
            self.encryptDecrypt.selectSegment(withTag: 0)
        }
    }
    
    @IBAction func keyGeneration(_ sender: NSButton) {
        let result = myRSA.keyGeneration()
        self.keyE.stringValue    = String(result.0)
        self.keyD.stringValue    = String(result.1)
        self.keyN.stringValue    = String(result.2)
        self.keyNTwo.stringValue = String(result.2)
    }
    
    @IBAction func runCalculations(_ sender: NSButton) {
        self.textOUT.stringValue = ""
        myRSA.d = Int(self.keyD.stringValue)!
        myRSA.e = Int(self.keyE.stringValue)!
        myRSA.N = Int(self.keyN.stringValue)!
        if self.encryptDecrypt.isSelected(forSegment: 0){
            self.textOUT.stringValue = myRSA.encrypt(text: self.textIN.stringValue)
        } else {
            self.textOUT.stringValue = myRSA.decode(text: self.textIN.stringValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    
}

